 	$('ul#filter_by li').find('a').click(function() {
		var postType = this.className;
		var count = 0;
		byCategory(postType);
		//alert(postType);
		return false;
	});
		
		function byCategory(postType, callback) {
		    jQuery.ajax({
			 type: "GET",
			 url: '/api/read/json?type=' + postType + 
				  '&callback=?',
			 dataType: "json",
			 success: function(results){
				 yourCallbackToRunIfSuccessful(results);
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown){
				 yourErrorHandler(XMLHttpRequest, textStatus, errorThrown);
			 }
		  });
       	
		  
		
		function yourCallbackToRunIfSuccessful(data) {  
		  var article = [];
			  $.each(data.posts, function(i, item) {
			  switch(item.type) {
			  case 'photo':
			  article[i] = '<div class="article_t"></div><div class="article">'
			  				+ '<a href="' + item.url + '" title="Elegantem"><img src="' 
							+ item['photo-url-500'] 
							+ '" class="alignleft" /></a><a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'video':
			  article[i] = '<div class="article_t"></div><div class="article">'
			  				+ '<div class="media_video" style="float: left; width: auto">' 
							+ item['video-player'] 
							+ '</div>' 
							+ '<a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'audio':
			  article[i] = '<div class="article_t"></div><div class="article">'
							+ '<h2><a href="'
			  				+ item.url
							+ '">'
							+ item['id3-artist'] 
							+' - '
							+ item['id3-title']
							+ '</a></h2><div class="player">'
							+ item['audio-player'] 
							+ '</div>'
							+ '<a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'regular':
			  article[i] = '<div class="article_t"></div><div class="article">' 
							+ '<h2><a href="'
							+ item.url 
							+ '">' 
							+ item['regular-title']
							+ '</a></h2>'
							+ item['regular-body'] 
							+ '<a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'quote':
			  article[i] = '<div class="article_t"></div><div class="article"><blockquote>'
							+ item['quote-text']
							+ '</blockquote><p class="quote_author">'
							+ item['quote-source'] 
							+ '</p><a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'conversation':
			  article[i] = '<div class="article_t"></div><div class="article">' 
							+ '<h2 style="padding-bottom: 0"><a href="' 
							+ item.url 
							+ '">'
							+ item['conversation-title']
							+ '</a></h2><a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'link':
			  article[i] = '<div class="article_t"></div><div class="article">' 
							+ '<h2><a href="'
							+ item['link-url'] 
							+ '" target="_blank">'
							+ item['link-url'] 
							+ ' - '
							+ item['link-text']
							+ '</a></h2><a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  case 'answer':
			  article[i] = '<div class="article_t"></div><div class="article">' 
							+ '<h2><a href="'
							+ item['link-url'] 
							+ '" target="_blank">'
							+ item['link-url'] 
							+ ' - '
							+ item['link-text']
							+ '</a></h2><a href="' + item.url + '" class="go_details" title="Details"></a></div><div class="article_b"></div>';
			  count = 1;
			  break;
			  default:
			  alert('No Entries Found.');
			  };
			  })

			  if (!(count == 0)) {
			  $('#content')
			  	.hide('fast')
			  	.html('<h1 class="page-title">Displaying ' 
				  + postType 
				  + ' posts only</h1>'
				  + article.join(''))
				.slideDown('fast')
				} else {
					$('#content')
					.hide('fast')
					.html('<div class="first_div"><span class="left_corner"></span><span class="right_corner"></span><h2>Hmmm, currently there are no ' 
					  + postType 
					  + ' posts to display</h2></div>')
					.slideDown('fast')
				};
			}; 
			
			function yourErrorHandler(data,textStatus,xhr) {
				$('#content').html('<div class="first_div"><span class="left_corner"></span><span class="right_corner"></span><h2>Hmmm, it seems the Tumblr Server is having some problems. Please try again later.</h2></div>')
			};
		};