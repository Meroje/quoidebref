<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Simply tell Laravel the HTTP verbs and URIs it should respond to. It's a
	| piece of cake to create beautiful applications using the elegant RESTful
	| routing available in Laravel.
	|
	| Let's respond to a simple GET request to http://example.com/hello:
	|
	|		'GET /hello' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| You can even respond to more than one URI:
	|
	|		'GET /hello, GET /world' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| It's easy to allow URI wildcards using (:num) or (:any):
	|
	|		'GET /hello/(:any)' => function($name)
	|		{
	|			return "Welcome, $name.";
	|		}
	|
	*/

	'GET /' => array('name' => 'home', 'do' => function()
	{
		$brefs = Bref::paginated();
		return View::of_layout()->nest('content', 'item.index', array('items' => $brefs));
	}),
	
	'GET /random' => array('name' => 'home', 'do' => function()
	{
		$q = "SELECT slug FROM brefs WHERE RAND()<(SELECT ((1/COUNT(*))*10) FROM brefs) AND `created_at`<NOW() ORDER BY RAND() LIMIT 1";
		
		$bref = DB::connection()->only($q);
		
		return Redirect::to_bref($bref);
	}),
	
	'GET /monbref' => array('name' => 'add', 'do' => function()
	{
		$categories = Categorie::as_array();
		$notification = Session::get('notification');
		return View::of_layout()->nest('content', 'item.add', array('categories' => $categories));
	}),
	
	'POST /monbref' => array('before' => 'csrf', 'do' => function()
	{
	    $bref = new Bref;

	    if (! $bref->create(Input::get()))
	    {
	        return Redirect::to_add()->with_errors($bref->validator)->with_input();
	    }

	    return Redirect::to_add()->with('notification', Lang::line('application.notifications.addSuccess')->get());
	}),
	
	'GET /bref' => function()
	{
		$brefs = Bref::paginated();
		return View::of_layout()->nest('content', 'item.index', array('items' => $brefs));
	},
	
	'GET /bref/(:any)' => array('name' => 'bref', 'do' => function($slug)
	{
		$bref = Bref::fromSlug($slug);
		return View::of_layout()->nest('content', 'item.view', array('item' => $bref));
	}),
	
	'GET /cat' => array('name' => 'cats', 'do' => function()
	{
		$categories = Categorie::order_by('created_at', 'desc')->paginate();
		return View::of_layout()->nest('content', 'item.index', array('items' => $categories));
	}),
	
	'GET /cat/(:any)' => array('name' => 'cat', 'do' => function($slug)
	{
		$categorie = Categorie::order_by('created_at', 'desc')->where('slug', '=', $slug)->first();
		return View::of_layout()->nest('content', 'item.view', array('item' => $categorie));
	}),
	
	'GET /tag/(:any)' => array('name' => 'tag', 'do' => function($slug)
	{
		$tag = Tag::where('nom', '=', $slug)->first();
		return View::of_layout()->nest('content', 'tag.view', array('tag' => $tag));
	}),
	
	'GET /blank' => function()
	{
		return null;
	}

);