<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Error Messages
	|--------------------------------------------------------------------------
	*/

	"accepted"   => "Les :attribute doivent être acceptées.",
	"active_url" => "L'adresse :attribute n'existe pas.",
	"alpha"      => "Le :attribute ne peut contenir que des lettres.",
	"alpha_dash" => "Le :attribute ne peut contenir que des lettres, nombres, tirets, et soulignements.",
	"alpha_num"  => "Le :attribute ne peut contenir que des lettres et des nombres.",
	"between"    => "Le :attribute doit être compris entre :min et :max.",
	"confirmed"  => "La confirmation de :attribute ne correspond pas.",
	"email"      => "Le format de :attribute est invalide.",
	"image"      => ":attribute doit être une image.",
	"in"         => "La valeur sélectionnée :attribute est invalide.",
	"integer"    => "Le :attribute doit être un nombre entier.",
	"max"        => "Le :attribute doit être inférieur à :max.",
	"mimes"      => "Le :attribute doit être un fichier de type: :values.",
	"min"        => "Le :attribute doit être supérieur à :min.",
	"not_in"     => "La valeur sélectionnée :attribute est invalide.",
	"numeric"    => "Le :attribute doit être un nombre.",
	"required"   => "Le champ :attribute est requis.",
	"size"       => "Le :attribute doit être :size.",
	"unique"     => "Le :attribute est déjà pris.",
	"url"        => "Le format de :attribute est invalide.",	

	/*
	|--------------------------------------------------------------------------
	| The following words are appended to the "size" messages when applicable,
	| such as when validating string lengths or the size of file uploads.
	|--------------------------------------------------------------------------
	*/

	"characters" => "caractères",
	"kilobytes"  => "kilobites",

);