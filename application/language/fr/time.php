<?php

return array(
	'second' => 'seconde',
	'minute' => 'minute',
	'hour' => 'heure',
	'day' => 'jour',
	'week' => 'semaine',
	'month' => 'month',
	'year' => 'année',
	'decade' => 'décennie',
);