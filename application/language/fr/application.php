<?php

return array(

	'header' => array(
		'title' => 'Quoi de Bref ?',
	),
	
	'top' => array(
		// Texte du lien pour la version mobile
		'mobile' => 'Mobile',
		
		// Texte du lien random
		'random' => 'Au hasard',
		
		// Texte du lien vers le flux rss
		'rss' => 'Derniers brefs',
		
		// Texte du lien vers les archives
		'archive' => 'Archives',
		
		// Texte du lien vers le formulaire de contact
		'ask' => 'Contact',
		
		// Texte du lien vers le formulaire d'ajout
		'submission' => 'Mon bref'
	),
	
	'aside' => array(
		// Filtres
		'postTypes' => array(
			'title' => 'Filtres',
			'article' => 'Article',
			'image' => 'Image',
			'link' => 'Lien',
			'audio' => 'Audio',
			'video' => 'Video',
			'quote' => 'Citation',
		),
		
		// Titre de la boite description
		'aboutMe' => 'Quoi de Bref ?',
		
		// Texte de description
		'description' => "Dans la vie, au début on naît, à la fin on meurt, entre les deux y s’passe des trucs. Bref. C’est l’histoire d’un mec, entre les deux. <br> Et Vous ?",
		
		// Titre de la boite réseaux sociaux
		'followMe' => 'Suivez-nous',
		
		// Titre de la boite des amis
		'following' => 'Nos amis',
		
		// Titre de la boite twitter
		'twittroll' => 'Twittroll',
	),
	
	'footer' => array(
		// Texte du footer
		'copyright' => '  © Copyright 2011. All rights reserved. Created by Dream-Theme &mdash; <a href="http://dream-theme.com/" target="_blank">premium tumblr and wordpress themes</a>. Powered by <a href="http://tumblr.com/" target="_blank">Tumblr</a>.',
	),
	
	// Libellés de formulaire
	'forms' => array(
		// Bouton envoyer
		'submit' => 'Valider',
		// Formulaire Bref
		'bref' => array(
			// Champ Titre
			'titre' => 'Entrez un titre pour votre bref. Doit commencer par "Bref,"',
			// Champ Auteur
			'auteur' => 'Entrez votre nom ou pseudo',
			//Champ Catégorie
			'categorie' => 'Sélectionnez une catégorie pour votre bref. Laissez à "Non classé" si aucune catégorie ne correspond.',
			// Champ Texte du Bref
			'texte' => 'Entrez le texte de votre bref. Doit se terminer par le titre.',
		),
		'admin' => array(
			'username' => 'Nom d\'utilisateur',
			'password' => 'Mot de passe',
			'password_confirmation' => 'Confirmer le mot de passe',
			'name' => 'Nom à afficher',
			'email' => 'Adresse email',
		),
	),
	
	// Titre des boites de contenu
	'titres' => array(
		// Titre de la boite d'ajout de Bref
		'addBref' => 'Proposer un nouveau bref',
	),
	
	// Texte des notifications
	'notifications' => array(
		// Ajout d'un Bref réussi
		'addSuccess' => 'Votre bref à été envoyé à l\'équipe de modération. Merci',
		// Suppression d'un User
		'userDel' => 'L\'utilisateur à bien été supprimé',
	),
	
	// Textes l!és à la partie commentaires
	'comments' => array(
		// Texte du lien de bas de boites de contenu
		'view' => 'Voir les commentaires',
		// Crédit Disqus
		'disqus' => 'Commentaires propulsés par Disqus',
	),
	
	// Textes variables
	'variables' => array(
		// Texte du temps relatif en bas de boites de contenu
		'timeAgo' => 'Posté il y a :time',
	),
	
);