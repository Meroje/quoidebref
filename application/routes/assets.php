<?php
return array(

	'GET /assets/style.css' => function()
	{
		return Basset::add('style', 'style.css')
					 ->add('day', 'day.css');
	},

	'GET /assets/cufon.js' => function()
	{
		return Basset::add('cufon-yui', 'cufon-yui.js')
					 ->add('cartogothic_std', 'cartogothic_std.font.js')
					 ->add('cartogothic_std_700', 'cartogothic_std_700.font.js');
	},
	
	'GET /assets/old_ie.css' => function()
	{
		return Basset::add('old_ie', 'old_ie.css');
	},

	'GET /assets/pie.js' => function()
	{
		return Basset::add('pie', 'pie.js');
	},
	
	'GET /assets/tweets.js' => function()
	{
		return Basset::add('tweets', "https://twitter.com/statuses/user_timeline/".Config::get('site.aside.twitterUsername').".json?callback=recent_tweets&count=".Config::get('site.aside.twitterCount'));
	},
	
	'GET /assets/social.css' => function()
	{
		return Basset::add('social', 'social.css');
	},
	
	'GET /assets/social.js' => function()
	{
		return Basset::add('social', 'jquery.sharrre-1.1.0.min.js');
	},
);