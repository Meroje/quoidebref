<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Simply tell Laravel the HTTP verbs and URIs it should respond to. It's a
	| piece of cake to create beautiful applications using the elegant RESTful
	| routing available in Laravel.
	|
	| Let's respond to a simple GET request to http://example.com/hello:
	|
	|		'GET /hello' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| You can even respond to more than one URI:
	|
	|		'GET /hello, GET /world' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| It's easy to allow URI wildcards using (:num) or (:any):
	|
	|		'GET /hello/(:any)' => function($name)
	|		{
	|			return "Welcome, $name.";
	|		}
	|
	*/

	'GET /admin/user' => array('before' => 'auth', 'do' => function()
	{
		$users = User::all();
		$notification = (Session::get('deleted')) ? Lang::line('application.notifications.userDel')->get() : NULL;
		return View::of_layout(array('content' => View::make('admin.user.index')->with('users', $users)->with('notification', $notification)));
	}),
	
	'GET /admin/user/edit/(:num)' => array('before' => 'auth', 'do' => function($id)
	{
		return View::of_layout(array('content' => View::make('admin.user.form')->with('user', User::find($id))));
	}),
	
	'POST /admin/user/edit/(:num)' => array('before' => 'auth', 'do' => function($id)
	{
	    $user = User::find($id);

	    if (! $user->create(Input::get(), false))
	    {
	        return Redirect::to('/admin/user/edit/'.$id)->with_errors($user->validator)->with_input();
	    }

	    return Redirect::to('/admin/user')->with('notification', Lang::line('application.notifications.userEdit')->get());
	}),
	
	'GET /admin/user/remove/(:num)' => array('before' => 'auth', 'do' => function($id)
	{
		return Redirect::to('/admin/user')->with('deleted', DB::table('users')->delete($id));
	}),

);