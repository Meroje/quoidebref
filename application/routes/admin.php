<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Simply tell Laravel the HTTP verbs and URIs it should respond to. It's a
	| piece of cake to create beautiful applications using the elegant RESTful
	| routing available in Laravel.
	|
	| Let's respond to a simple GET request to http://example.com/hello:
	|
	|		'GET /hello' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| You can even respond to more than one URI:
	|
	|		'GET /hello, GET /world' => function()
	|		{
	|			return 'Hello World!';
	|		}
	|
	| It's easy to allow URI wildcards using (:num) or (:any):
	|
	|		'GET /hello/(:any)' => function($name)
	|		{
	|			return "Welcome, $name.";
	|		}
	|
	*/

	'GET /admin' => array('name' => 'admin', 'before' => 'auth', 'do' => function()
	{
		$users = User::all();
		return View::of_layout(array('content' => View::make('admin.index')->with('users', $users)));
	}),
	
	/*
	|--------------------------------------------------------------------------
	| Authentication Routes
	|--------------------------------------------------------------------------
	*/
	
	'GET /admin/login' => array('name' => 'login', 'before' => 'guest', 'do' => function()
	{
		return View::of_layout(array('content' => View::make('admin.login')));
	}),
	
	'POST /admin/login' => array('before' => 'guest', 'do' => function()
	{
		$redirect = Session::get('redirect', '/admin');

		if (Auth::attempt(Input::get('username'), Input::get('password'), 1))
		{
			return Redirect::to($redirect);
		}

		return Redirect::to_login()->with_errors(Lang::line('application.admin.loginFail')->get())->with_input('except', array('password'));
	}),

	'GET /admin/logout' => array('name' => 'logout', 'do' => function()
	{
		Auth::logout();

		return Redirect::to_home();
	}),

	'GET /admin/register' => array('name' => 'register', 'before' => 'guest', 'do' => function()
	{
		return View::of_layout(array('content' => View::make('admin.register')));
	}),

	'POST /admin/register' => array('before' => 'csrf', 'do' => function()
	{
	    $user = User::where_key_and_email(Input::get('key'), Input::get('email'))->where_not_null('key')->first();
		if (empty($user))
		{
			return Redirect::to_badKey()->with_input('only', array('email', 'key'));
		}
		else if (! $user->create(Input::get()))
	    {
	        return Redirect::to_register()->with_errors($user->validator)->with_input('except', array('password', 'password_confirmation'));
	    }

	    return Redirect::to_admin()->with('notification', Lang::line('application.admin.registerSuccess')->get());
	}),
	
	'GET /admin/bad' => array('name' => 'badKey', 'do' => function()
	{
		return "Bad Key " . Input::old('key') . " for email " . Input::old('email');
	}),
	
	'GET /admin/slug' => function()
	{
		return Bref::makeSlugs();
	},

);