<?php

return array(
	// Nom d'utilisateur disqus
	'disqus' => 'quoidebref',
	
	// Configuration de l'en tête
	'header' => array(
		// Utiliser cufon pour le rendu des polices ?
		'enableCufon' => true,
	
		// Couleur de fond
		'backgroundColor' => '#fff',
	
		// Image de fond
		'backgroundPattern' => '/img/patterns/4_4_dots-small-light-no-micro.png',
	
		// Image de fond du titre
		'backgroundImage' => null,
	),
	
	//Configuration de la partie haute
	'top' =>array(
		// Version mobile active ?
		'mobile' => false,
		
		// Formulaire de contact actif ?
		'askEnabled' => false,
		
		// Logo du site
		'logoImage' => '/img/logo.png',
		
		// Pages
		'pages' => array(
			'Accueil' => '/',
			'Mon Bref !' =>  '/monbref',
			'Brefs' => '/bref',
			'Catégories' => '/cat',
			'Blog' => 'http://blog.quoidebref.com/',
			'Vidéos' => 'http://blog.quoidebref.com/tagged/videos',
			'Aide' => '/help',
		),
	),
	
	// Configuration de la barre latérale
	'aside' => array(
		// Montrer les filtres ?
		'showPostTypeFilter' => false,
		
		// Montrer la description ?
		'showDescription' => true,
		
		// Lien vers le petit logo pour la description
		'logoSmall' => '/img/logo_small_noir.png',
		
		// Montrer les liens vers les réseaux sociaux ?
		'showSocialLinks' => true,
		
		// Liens vers les réseaux sociaux
		'social' => array(
			'tumblrLink' => '',
			'facebookLink' => 'http://facebook.com/quoidebref',
			'lastfmLink' => '',
			'twitterLink' => 'http://twitter.com/quoidebref',
			'youTubeLink' => '',
			'flickrLink' => '',
			'myspaceLink' => '',
			'vimeoLink' => '',
		),
		
		// ID Flickr, conditionne l'affichage de la boite flickr
		'flickrID' => '',
		
		// Montrer les amis
		'showPeopleIFollow' => false,
		
		// Montrer les tweets
		'showTwitter' => true,
		
		// Nom d'utilisateur twitter, requis si showTwitter est à true
		'twitterUsername' => 'quoidebref',
		
		// Nombre de tweets à afficher
		'twitterCount' => 5,
		
		// Montrer la boit recherche ?
		'showSearch' => false,
	),
);
