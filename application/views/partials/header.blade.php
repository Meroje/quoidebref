<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--[if gte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=8"/><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{Lang::line('application.header.title')->get()}}</title>

<link href="/assets/style.css" rel="stylesheet" type="text/css">
<link href="/assets/social.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]><link href="/assets/old_ie.css" rel="stylesheet" type="text/css"><![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="/assets/social.js"></script>
@if (Config::get('site.header.enableCufon'))
<script src="/assets/cufon.js" type="text/javascript"></script>
<script type="text/javascript">
Cufon('#nav > li > a', {
	color: '-linear-gradient(#272727, #1d1d1d)', textShadow: '1px 1px #fff',
	hover: {
		color: '-linear-gradient(#414141, #606060)', textShadow: '1px 1px #fff'
	}
});
Cufon('h1, h2, h3, h4, h5, h6, .quote_author', {
	color: '-linear-gradient(#282828, #1d1d1d)', textShadow: '1px 1px #fff'
});
Cufon('.post_type div, .post_type span, .header', {
	color: '-linear-gradient(#fff, 0.4=#e8eaeb, #b0b5b8)', textShadow: '1px 1px #000'
});
</script>
@endif

<script type="text/javascript" src="/assets/pie.js"></script>
<script type="text/javascript">
$(function() {
    $('.shadow_light, .shadow_dark, .alignnone, .alignleft, .alignright, .aligncenter, .media_video').each(function() {
        PIE.attach(this);
    });
});
</script>

<style type="text/css">
html{
	background: {{Config::get('site.header.backgroundColor')}} url({{Config::get('site.header.backgroundPattern')}}) repeat center top;
}
body{
	background: url({{Config::get('site.header.backgroundImage')}}) no-repeat center top;
}
</style>
</head>