{block:SearchPage}
  <div class="article_t"></div>
  <div class="article">
    {block:SearchResults}
      <h1>{lang:Search}</h1>
      {lang:SearchResultCount results for SearchQuery}
    {/block:SearchResults}

    {block:NoSearchResults}
      <h1>{lang:Search}</h1>
      {lang:No search results for SearchQuery}
    {/block:NoSearchResults}
  </div>
  <div class="article_b"></div>
{/block:SearchPage}