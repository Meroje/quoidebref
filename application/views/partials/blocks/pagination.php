{block:Pagination}  
  <ul class="paginator">
    {block:PreviousPage}<li class="larr"><a href="{PreviousPage}"><span>{lang:Newer posts}</span></a></li>{/block:PreviousPage}

    {block:JumpPagination length="5"} 
    {block:CurrentPage}<li class="act"><a href="{URL}">{PageNumber}</a></li>{/block:CurrentPage}
    {block:JumpPage}<li><a href="{URL}">{PageNumber}</a></li>{/block:JumpPage}
    {/block:JumpPagination}

    {block:NextPage}<li class="rarr"><a href="{NextPage}"><span>{lang:Older posts}</span></a></li>{/block:NextPage}
  </ul>
{/block:Pagination}