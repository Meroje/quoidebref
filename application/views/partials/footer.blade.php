<div id="footer_shadow"></div>

<div id="footer_bg">
<div id="footer_s">
<div id="footer">
{{Lang::line('application.footer.copyright')->get()}}
</div>
</div>
</div>

</div>

<script type="text/javascript" src="/assets/tweets.js"></script>
<script>
@if ($index)
$('.shareindex').sharrre({
  share: {
    twitter: true,
    facebook: true,
    googlePlus: true
  },
  template: '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook">f</a><a href="#" class="twitter">t</a><a href="#" class="googleplus">+1</a></div><div class="right">{total}</div></div>',
  urlCurl: '/sharrre.php',
  enableHover: false,
  enableTracking: true,
  render: function(api, options){
    $('.twitter').on('click', function() {
      api.openPopup('twitter');
    });
    $('.facebook').on('click', function() {
      api.openPopup('facebook');
    });
    $('.googleplus').on('click', function() {
      api.openPopup('googlePlus');
    });
  }
});
@else
$('#twitter').sharrre({
  share: {
    twitter: true
  },
  template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Tweet</div></a>',
  enableHover: false,
  enableTracking: true,
  buttons: { twitter: {via: 'quoidebref'}},
  click: function(api, options){
    api.openPopup('twitter');
  }
});
$('#facebook').sharrre({
  share: {
    facebook: true
  },
  template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Like</div></a>',
  enableHover: false,
  enableTracking: true,
  click: function(api, options){
    api.openPopup('facebook');
  }
});
$('#googleplus').sharrre({
  share: {
    googlePlus: true
  },
  template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>+1</div></a>',
  urlCurl: '/sharrre.php',
  enableHover: false,
  enableTracking: true,
  click: function(api, options){
    api.openPopup('googlePlus');
  }
});
@endif
</script>
</body>
</html>