</div>

<div id="aside">

@if (Config::get('site.aside.showPostTypeFilter'))
  <div class="widget">
    <div class="header">{{Lang::line('application.aside.postTypes.title')->get()}}</div>
    <ul id="filter_by" class="categories type">
      <li class="first text"><a class="regular" href="#">{{Lang::line('application.aside.postTypes.article')->get()}}</a></li>
      <li class="first image"><a class="photo" href="#">{{Lang::line('application.aside.postTypes.image')->get()}}</a></li>
      <li class="link"><a class="link" href="#">{{Lang::line('application.aside.postTypes.link')->get()}}</a></li>
      <li class="audio"><a class="audio" href="#">{{Lang::line('application.aside.postTypes.audio')->get()}}</a></li>
      <li class="video"><a class="video" href="#">{{Lang::line('application.aside.postTypes.video')->get()}}</a></li>
      <li class="quote"><a class="quote" href="#">{{Lang::line('application.aside.postTypes.quote')->get()}}</a></li>
    </ul>
  </div>
  <div class="widget_b"></div>
@endif

@if (Config::get('site.aside.showDescription'))
  <div class="widget">
    <div class="header">{{Lang::line('application.aside.aboutMe')->get()}}</div>
    @if (Config::get('site.aside.logoSmall'))
	  <img src="{{Config::get('site.aside.logoSmall')}}" width="60" height="60" class="alignleft" />
	@endif
    {{Lang::line('application.aside.description')->get()}}
  </div>
  <div class="widget_b"></div>
@endif
  
@if (Config::get('site.aside.showSocialLinks'))
  <div class="widget">
    <div class="header">{{Lang::line('application.aside.followMe')->get()}}</div>
    <div class="soc_hold"><ul class="categories social">
	  @if (Config::get('site.aside.social.tumblrLink'))
      	<li class="tumblr"><a href="{{Config::get('site.aside.social.tumblrLink')}}" target="_blank"><i></i>Tumblr</a></li>
	  @endif
      @if (Config::get('site.aside.social.facebookLink'))
		<li class="facebook"><a href="{{Config::get('site.aside.social.facebookLink')}}" target="_blank"><i></i>Facebook</a></li>
	  @endif
      @if (Config::get('site.aside.social.lastfmLink'))
		<li class="lastfm"><a href="{{Config::get('site.aside.social.lastfmLink')}}" target="_blank"><i></i>Last.fm</a></li>
	  @endif
      @if (Config::get('site.aside.social.twitterLink'))
		<li class="twitter"><a href="{{Config::get('site.aside.social.twitterLink')}}" target="_blank"><i></i>Twitter</a></li>
	  @endif
      @if (Config::get('site.aside.social.youTubeLink'))
		<li class="youtube"><a href="{{Config::get('site.aside.social.youTubeLink')}}" target="_blank"><i></i>Youtube</a></li>
	  @endif
      @if (Config::get('site.aside.social.flickrLink'))
		<li class="flickr"><a href="{{Config::get('site.aside.social.flickrLink')}}" target="_blank"><i></i>Flickr</a></li>
	  @endif
      @if (Config::get('site.aside.social.myspaceLink'))
		<li class="myspace"><a href="{{Config::get('site.aside.social.myspaceLink')}}" target="_blank"><i></i>Myspace</a></li>
	  @endif
      @if (Config::get('site.aside.social.vimeoLink'))
		<li class="vimeo"><a href="{{Config::get('site.aside.social.vimeoLink')}}" target="_blank"><i></i>Vimeo</a></li>
	  @endif
    </ul></div>
  </div>
  <div class="widget_b"></div>
@endif
  
  @if (Config::get('site.aside.flickrID'))
  <div class="widget">
    <div class="header">Flickr</div>
    <div class="flickr">
      <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&display=latest&size=s&layout=x&source=user&user={{Config::get('site.aside.flickrID')}}"></script>
    </div>
  </div>
  <div class="widget_b"></div>
  @endif
  
  @if (Config::get('site.aside.showPeopleIFollow'))
  <div class="widget">
    <div class="header">{{Lang::line('application.aside.following')->get()}}</div>
    <div class="flickr">
	  <!-- Foreach followed -->
      <a href="{{$followedURL}}"><img src="{{$followedPortraitURL}}" width="46" height="46" class="alignleft" alt="{{$followedName}}: {{$followedTitle}}" /></a>
    </div>
  </div>
  <div class="widget_b"></div>
  @endif

  @if (Config::get('site.aside.showTwitter'))
  <div class="widget">
    <div class="header">{{Lang::line('application.aside.twittroll')->get()}}</div>
      <div id="tweets">
      </div>
  </div>
  <div class="widget_b"></div>
  <script type="text/javascript">
      function recent_tweets(data) {
            for (i=0; i<data.length; i++) {
                document.getElementById("tweets").innerHTML = 
                    document.getElementById("tweets").innerHTML + 
                    '<div class="post"><a href="http://twitter.com/{{Config::get('site.aside.twitterUsername')}}/status/' + 
                    (data[i].id_str ? data[i].id_str : data[i].id) +
                    '">' + data[i].text + 
                    '</a><span class="ico_link date">' + data[i].created_at + '</span></div>';
            }
        }
    </script>
  @endif
  
  @if (Config::get('site.aside.showSearch'))
  <div class="widget">
    <div class="header">Search</div>
    <form action="/search" method="post" name="search_form" id="search_form" class="c_search">
      <input type="text" name="query" value="$searchQuery"/>
      <a href="javascript: document.search_form.submit();"></a>
    </form>
  </div>
  <div class="widget_b"></div>
  @endif

</div>
</div>