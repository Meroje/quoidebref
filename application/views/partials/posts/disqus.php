{block:IfDisqusShortname}
	{block:Permalink}
	  <div class="notecontainer">
		<div id="disqus_thread"><div class="note_line"></div></div>
		<script type="text/javascript" src="http://disqus.com/forums/{text:Disqus Shortname}/embed.js"></script>
		<noscript><a href="http://{text:Disqus Shortname}.disqus.com/?url=ref">{lang:View the discussion thread}</a></noscript>
	  </div>
	  <div style="text-align:right; margin-top:5px;">
		{lang:Blog comments powered by Disqus 2}
	  </div>
	{/block:Permalink}
{/block:IfDisqusShortname}