{block:Date} 
<div class="article_footer">
	<div class="article_footer_s">
	  <a href="{Permalink}" class="ico_link permalink">
		{block:Reblog}
		  {lang:Reblogged TimeAgo from ReblogParentName} {block:RebloggedFromReblog}(<span style="text-transform:lowercase;">{lang:Originally from ReblogRootName}</span>){/block:RebloggedFromReblog}
		{/block:Reblog}
									
		{block:NotReblog}
		  {lang:Posted TimeAgo from source}
		{/block:NotReblog}
	  </a>
	  {block:IfDisqusShortname}<a href="{Permalink}#disqus_thread" class="ico_link comments">{lang:View comments}</a>{/block:IfDisqusShortname}
	  {block:HasTags}<span class="ico_link tags">{block:Tags}<a href="{TagURL}">{Tag}</a>, {/block:Tags}</span>{/block:HasTags}
	  {block:NoteCount}<a href="{Permalink}#notes_list" class="ico_link notes">{NoteCountWithLabel}</a>{/block:NoteCount}
	</div>
</div>
<div class="article_footer_b"></div>
{/block:Date}