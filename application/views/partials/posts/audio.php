{block:Audio}
	<div class="a_player">
	  {block:AlbumArt}
	  <div class="album_art">
		<img class="alignleft" src="{AlbumArtURL}" alt="{block:Artist}{Artist}{/block:Artist}{block:TrackName} - {TrackName}{/block:TrackName}" />
	  </div>
	  {/block:AlbumArt}
	  <div class="audio">
		{block:Artist}<p><span class="grey_light">Artist:</span> {Artist}</p>{/block:Artist}
		{block:Album}<p><span class="grey_light">Album:</span> {Album}</p>{/block:Album}
		{block:TrackName}<p><span class="grey_light">Track:</span> {TrackName}</p>{/block:TrackName}
		<div class="media_audio">{AudioPlayerGrey}</div>
		<p class="grey_light">{PlayCountWithLabel}{block:ExternalAudio}<span class="download_external_audio"> &bull; <a href="{ExternalAudioURL}">{lang:Download}</a></span>{/block:ExternalAudio}</p>
	  </div>
	</div>
	{Caption}
{/block:Audio}