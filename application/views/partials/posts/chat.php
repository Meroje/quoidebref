{block:Chat}
	<table class="chat">
	  {block:Lines}
	  <tr class="{Alt}">
		{block:Label}<td class="author">
		  {Label}
		</td>{/block:Label}
		<td>
		  {Line}
		</td>
	  </tr>
	  <tr class="spread"><td></td><td></td></tr>
	  {/block:Lines}
	</table>
{/block:Chat}