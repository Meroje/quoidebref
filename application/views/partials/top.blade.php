<body>
<div id="bg">

<div id="top">
  <div>
    @if (Config::get('site.top.mobile'))
		<a href="/mobile" class="ico_mobile">{{Lang::line('application.top.mobile')->get()}}</a>
	@endif
    <a href="/random" class="ico_random">{{Lang::line('application.top.random')->get()}}</a>
    <a href="/rss" class="ico_rss">{{Lang::line('application.top.rss')->get()}}</a>
    <a href="/archive" class="ico_archive">{{Lang::line('application.top.archive')->get()}}</a>
    @if (Config::get('site.top.askEnabled'))
		<a href="/ask" class="ico_ask">{{Lang::line('application.top.ask')->get()}}</a>
	@endif
    @if (Config::get('site.top.submissionsEnabled'))
		<a href="/submit" class="ico_submit">{{Config::get('site.top.submission')}}</a>
	@endif
  </div>
</div>

<div id="header">
  <a href="/" id="logo">@if ($logo = Config::get('site.top.logoImage'))<img src="{{$logo}}" />@endif</a>
  @if (Config::get('site.top.pages'))
  <ul id="nav">
    @foreach (Config::get('site.top.pages') as $label=>$url)<li><a href="{{$url}}">{{$label}}</a></li>@endforeach
  </ul>
  @endif
</div>

<div id="holder">
<div id="content">