<div class="article">
	<h1>{{Lang::line('application.admin.login')->get()}}</h1>
	{{Form::open()}}

		{{Form::text('username', Input::old('username'))}}
		{{Form::label('username', Lang::line('application.forms.admin.username')->get())}}<br>

		{{Form::password('password')}}
		{{Form::label('password', Lang::line('application.forms.admin.password')->get())}}<br>

		{{Form::token()}}
		{{Form::submit(Lang::line('application.forms.bref.submit')->get())}}
	{{Form::close()}}

	@if (!empty($errors->messages))
		<ul>
		@foreach ($errors->all('<li>:message</li>') as $error)
			{{$error}}
		@endforeach
		</ul>
	@endif
</div>
<div class="article_b"></div>