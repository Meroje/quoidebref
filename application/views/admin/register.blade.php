<div class="article">
	<h1>{{Lang::line('application.admin.register')->get()}}</h1>
	{{Form::open()}}
		
		{{Form::text('name', Input::old('name'))}}
		{{Form::label('name', Lang::line('application.forms.admin.name')->get())}}<br>
		
		{{Form::text('username', Input::old('username'))}}
		{{Form::label('username', Lang::line('application.forms.admin.username')->get())}}<br>
		
		{{Form::text('email', Input::old('email'))}}
		{{Form::label('email', Lang::line('application.forms.admin.email')->get())}}<br>
		
		{{Form::password('password')}}
		{{Form::label('password', Lang::line('application.forms.admin.password')->get())}}<br>
		
		{{Form::password('password_confirmation')}}
		{{Form::label('password_confirmation', Lang::line('application.forms.admin.password_confirmation')->get())}}<br>

		{{Form::hidden('key', Input::get('key'))}}
		{{Form::token()}}
		{{Form::submit(Lang::line('application.forms.submit')->get())}}
	{{Form::close()}}
	
	@if (!empty($errors->messages))
		<ul>
		@foreach ($errors->all('<li>:message</li>') as $error)
			{{$error}}
		@endforeach
		</ul>
	@endif
</div>
<div class="article_b"></div>