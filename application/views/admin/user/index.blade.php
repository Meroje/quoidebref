		   <div class="article">
			  <h1>Administration</h1>
			  <h2>Users</h2>
			@if($notification)
			<p class="notification">
				{{$notification}}
			</p>	
			@endif
			<table class="chat">
			  <tbody>
			  <tr class="odd">
			    <td>Name</td>
			    <td>Username</td>
			    <td>Email</td>
				<td>Actions</td>
			  </tr>
		      <tr class="spread"><td></td><td></td><td></td><td></td></tr>
			  @foreach($users as $user)
			  <tr class="even">
				<td class="author">
				  {{$user->name}}
				</td>
				<td>
				  {{$user->username}}
				</td>
				<td>
				  {{$user->email}}
				</td>
				<td>
				  <ul>
				  	<li>{{HTML::link('admin/user/edit/'.$user->id, 'Edit')}}</li>
					<li>{{HTML::link('admin/user/remove/'.$user->id, 'Remove')}}</li>
				  </ul>
				</td>
			  </tr>
			  @endforeach
			  </tbody>
			</table>
		  </div>
		  <div class="article_b"></div>