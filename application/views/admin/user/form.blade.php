<div class="article">
	<h1>{{Lang::line('application.admin.user.edit')->get()}}</h1>
	{{Form::open()}}
		
		{{Form::text('name', $user->name)}}
		{{Form::label('name', Lang::line('application.forms.register.name')->get())}}<br>
		
		{{Form::text('username', $user->username)}}
		{{Form::label('username', Lang::line('application.forms.register.username')->get())}}<br>
		
		{{Form::text('email', $user->email)}}
		{{Form::label('email', Lang::line('application.forms.register.email')->get())}}<br>
		
		{{Form::password('password')}}
		{{Form::label('password', Lang::line('application.forms.admin.password')->get())}}<br>
		
		{{Form::password('password_confirmation')}}
		{{Form::label('password_confirmation', Lang::line('application.forms.admin.password_confirmation')->get())}}<br>

		{{Form::token()}}
		{{Form::submit(Lang::line('application.forms.submit')->get())}}
	{{Form::close()}}
	
	@if (!empty($errors->messages))
		<ul>
		@foreach ($errors->all('<li>:message</li>') as $error)
			{{$error}}
		@endforeach
		</ul>
	@endif
</div>
<div class="article_b"></div>