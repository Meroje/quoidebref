		  <div class="article_t">
		  	<div class="post_type quote">
		  	  <a href="{{URL::to_bref($item->slug)}}"></a>
		  	  <div>{{strftime('%e', strtotime($item->created_at))}}</div>
		  	  <span>{{strftime('%h', strtotime($item->created_at))}}</span>
		  	</div>
		  </div>
		  <div class="article">
			@if ($index)
			  <h2><a href="{{URL::to_bref($item->slug)}}">{{$item->titre}}</a></h2>
			@else
			  <h1>{{$item->titre}}</h1>
			@endif
			<p>{{$item->texte}}</p>
			@if ($index)
			<a href="{{URL::to_bref($item->slug)}}" class="go_details" title="Details"></a>
			<div id="share{{$item->id}}" class="shareindex" data-url="{{URL::to_bref($item->slug)}}" data-text="{{$item->titre}}"></div>
			@endif
			@if (Config::get('site.disqus') && !$index)
				  <div class="notecontainer">
					<div id="share{{$item->id}}" class="shareview">
					  <div id="twitter" data-url="{{URL::to_bref($item->slug)}}" data-text="{{$item->titre}}"></div>
					  <div id="facebook" data-url="{{URL::to_bref($item->slug)}}" data-text="{{$item->titre}}"></div>
					  <div id="googleplus" data-url="{{URL::to_bref($item->slug)}}" data-text="{{$item->titre}}"></div>
					</div>
					<div id="disqus_thread"><div class="note_line"></div></div>
					<script type="text/javascript" src="http://disqus.com/forums/{{Config::get('site.disqus')}}/embed.js"></script>
					<noscript><a href="http://{{Config::get('site.disqus')}}.disqus.com/?url=ref">{{Lang::line('application.comments.noscript')->get()}}</a></noscript>
				  </div>
				  <div style="text-align:right; margin-top:5px;">
					{{Lang::line('application.comments.disqus')->get()}}
				  </div>
			@endif
		  </div>
		  <div class="article_b"></div>

		<div class="article_footer">
			<div class="article_footer_s">
			  <a href="{{URL::to_bref($item->slug)}}" class="ico_link permalink">
				  {{Lang::line('application.variables.timeAgo', array('time' => $item->timeAgo()))}}
			  </a>
			  @if (Config::get('site.disqus'))
				<a href="{{URL::to_bref($item->slug)}}#disqus_thread" class="ico_link comments">{{Lang::line('application.comments.view')->get()}}</a>
			  @endif
			  @if ($item->tags)
			    <span class="ico_link tags">
				  @foreach ($item->tags as $tag)
					<a href="{{URL::to_tag($tag->nom)}}">{{$tag->nom}}</a>,
				  @endforeach
				</span>
			  @endif
			  @if ($item->similaires)
				<a href="{{URL::to_bref($item->slug)}}#notes_list" class="ico_link notes">{NoteCountWithLabel}</a>
			  @endif
			</div>
		</div>
		<div class="article_footer_b"></div>