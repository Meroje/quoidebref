<div class="article">
	<h2><a href="{{URL::to_add()}}">{{Lang::line('application.titres.addBref')->get()}}</a></h2>
	@if ($notification)
		<p>{{$notification}}</p>
	@endif
	{{Form::open()}}
		
		{{Form::text('titre', Input::old('titre'))}}
		{{Form::label('titre', Lang::line('application.forms.bref.titre')->get())}}<br>
		
		{{Form::text('auteur', Input::old('auteur'))}}
		{{Form::label('auteur', Lang::line('application.forms.bref.auteur')->get())}}<br>
		
		{{Form::select('categorie', $categories, Input::old('categorie'))}}
		{{Form::label('categorie', Lang::line('application.forms.bref.categorie')->get())}}<br>
		
		{{Form::textarea('texte', Input::old('texte'))}}
		{{Form::label('texte', Lang::line('application.forms.bref.texte')->get())}}<br>

		{{Form::token()}}
		{{Form::submit(Lang::line('application.forms.submit')->get())}}
	{{Form::close()}}
	@if (!empty($errors->messages))
		<ul>
		@foreach ($errors->all('<li>:message</li>') as $error)
			{{$error}}
		@endforeach
		</ul>
	@endif
</div>
<div class="article_b"></div>