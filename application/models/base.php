<?php
class Base extends Eloquent {

    /**
     * @var Laravel\Validator
     */
    public $validator = null;
    
    /**
     * @var array
     * 
     * The input array
     */
    public $data = array();
    
    /**
     * Validates the input
     * 
     * @param array    $data
     * @return boolean
     */
    public function valid()
    {    
        // create the validator
        $this->validator = Validator::make($this->data, $this->rules);
        
        // return if the validator passed
        return $this->validator->valid();
    }

	public function timeAgo()
	{
	   $periods = array(Lang::line("time.second"), Lang::line("time.minute"), Lang::line("time.hour"), Lang::line("time.day"), Lang::line("time.week"), Lang::line("time.month"), Lang::line("time.year"), Lang::line("time.decade"));
	   $lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

	       $difference     = $now - strtotime($this->created_at);

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	       $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
	       $periods[$j].= "s";
	   }

	   return "$difference $periods[$j]";
	}
}