<?php
class Categorie extends Base {
	
	public static $timestamps = true;
	
	public static $per_page = 10;
	
	public function brefs()
	{
		return $this->has_many('Bref');
	}
	
	public function slug()
	{
		return $this->slug = URL::Slug($this->titre);
	}
	
	static function as_array()
	{
		$cats = static::all();
		$categories = array();
		foreach ($cats as $cat) {
			$categories[$cat->id] = $cat->titre;
		}
		return $categories;
	}
}