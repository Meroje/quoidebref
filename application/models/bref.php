<?php
class Bref extends Base {
	
	public static $timestamps = true;
	
	public static $per_page = 10;
	
	public $rules = array(
		'titre'		   => array('required'),
		'auteur'	   => array('required', 'alpha_dash'),
		'texte'		   => array('required'),
		'categorie'    => array('integer'),
		'texte'        => array('required')
	);
	
	public function categorie()
	{
		return $this->belongs_to('Categorie');
	}
	
	public function tags()
	{
		return $this->has_and_belongs_to_many('Tag');
	}
	
	public function create($data)
	{
	    $this->data = $data;

	    // if the validation fails
	    if (! $this->valid())
	    {
	        return false;
	    }

		$this->titre = $this->data['titre'];
		$this->slug = $this->sluggy();
		$this->auteur = $this->data['auteur'];
		$this->texte = $this->data['texte'];
		$this->categorie_id = $this->data['categorie'];
		
	    if ($this->save())
	    {
			return true;
		}
		
		return false;
	}
	
	public static function paginated()
	{
		return self::order_by('created_at', 'desc')->where_supprime_and_approuve('O', '1')->where('created_at', '<', DB::raw('NOW()'))->paginate();
	}
	
	public static function fromSlug($slug)
	{
		return self::order_by('created_at', 'desc')->where('slug', '=', $slug)->first();
	}
	
	public function sluggy()
	{
		$slug = URL::slug($this->titre);
		if(DB::table('brefs')->where('slug', '=', $slug)->first() === NULL)
		{
        	return $slug;
      	}
      	$i = 1;
      	while (DB::table('brefs')->where('slug', '=', $slug.'-'.$i)->first() !== NULL)
		{ 
        	$i++;
      	}
      	return $slug.'-'.$i;
	}
	
	public static function makeSlugs()
	{
		$slugs = self::where_null('slug')->get();
		foreach ($slugs as $slug)
		{
			$slug->slug = $slug->sluggy();
			$slug->save();
		}
		return "Done";
	}

}