<?php
class User extends Base {
	
	public static $timestamps = true;
	
	public $rules = array(
		'name'         => array('required', 'max:50'),
		'username'     => array('required', 'alpha_dash', 'unique:users'),
		'password'     => array('required', 'alpha_dash', 'confirmed'),
		'email'	       => array('required', 'email'),
		'key'          => array('required'),
	);
	
	public function create($data, $password = true)
	{
		if (!$password)
		{
			$this->rules['username'] = array('required', 'alpha_dash');
			$this->rules['password'] = array('alpha_dash', 'confirmed');
			unset($this->rules['key']);
		}
		
	    $this->data = $data;

	    // if the validation fails
	    if (! $this->valid())
	    {
	        return false;
	    }

		$this->name = $this->data['name'];
		$this->username = $this->data['username'];
		$this->email = $this->data['email'];
		$this->password = Hash::make($this->data['password']);
		$this->key = $this->data[NULL];
		
	    if ($this->save())
	    {
			return true;
		}
		
		return false;
	}

}