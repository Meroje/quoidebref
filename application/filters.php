<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Filters
	|--------------------------------------------------------------------------
	|
	| Filters provide a convenient method for attaching functionality to your
	| routes. Filters can run either before or after a route is exectued.
	|
	| The built-in "before" and "after" filters are called before and after
	| every request to your application; however, you may create other filters
	| that can be attached to individual routes.
	|
	| Filters also make common tasks such as authentication and CSRF protection
	| a breeze. If a filter that runs before a route returns a response, that
	| response will override the route action.
	|
	| Let's walk through an example...
	|
	| First, define a filter:
	|
	|		'simple_filter' => function()
	|		{
	|			return 'Filtered!';
	|		}
	|
	| Next, attach the filter to a route:
	|
	|		'GET /' => array('before' => 'simple_filter', function()
	|		{
	|			return 'Hello World!';
	|		})
	|
	| Now every requests to http://example.com will return "Filtered!", since
	| the filter is overriding the route action by returning a value.
	|
	| To make your life easier, we have built authentication and CSRF filters
	| that are ready to attach to your routes. Enjoy.
	|
	*/

	/**
	 * This before filter should not be altered. This method will set Basset to combine files
	 * by default regardless of what you have specified in the config.
	 */
	'before' => function()
	{
		Basset::routed();
	},


	/**
	 * If you append a file extension such as .css or .js to the end of your routes then it will
	 * automatically set the content type instead of manually specifying an after filter.
	 */
	'after' => function($response)
	{
		Input::flash();

		$uri = URI::current();
		$session = IoC::core('session');
		$types = array(
			'css' 	=> 'text/css',
			'less'	=> 'text/css',
			'js'	=> 'text/javascript'
		);
		
		if(strrpos($uri, '.') !== false && ($ext = \File::extension($uri)))
		{
			$session->reflash();
			$response->header('Content-Type', $types[$ext]);
		}
	},

	'auth' => function()
	{
		Session::put('redirect', URI::current());
		if (Auth::guest()) return Redirect::to_login();
	},

	'guest' => function()
	{
		if (!Auth::guest()) return Redirect::to_home();
	},

	'csrf' => function()
	{
		if (Request::forged()) return Response::error('500');
	},

	/**
	 * Manual CSS after filter.
	 */
	'css' => function($response)
	{
		$response->header('Content-Type', 'text/css');
	},

	/**
	 * Manual LESS after filter.
	 */
	'less' => function($response)
	{
		$response->header('Content-Type', 'text/css');
	},

	/**
	 * Manual JS after filter.
	 */
	'js' => function($response)
	{
		$response->header('Content-Type', 'text/javascript');
	},

);